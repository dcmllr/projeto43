
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Daniele Claudine Muller
 */
public class Pratica43 {
    public static void main(String[] args) {
        Circulo a = new Circulo(7);
        Elipse b = new Elipse(6, 4);
        Quadrado c = new Quadrado(3);
        Retangulo d = new Retangulo(3, 4);
        TrianguloEquilatero e = new TrianguloEquilatero(4);
        
        System.out.println("Area do circulo:" + a.getArea());
        System.out.println("Perimetro do circulo:" + a.getPerimetro());
        
        System.out.println("Area da elipse:" + b.getArea());
        System.out.println("Perimetro da elipse:" + b.getPerimetro());
        
        System.out.println("Area do quadrado:" + c.getArea());
        System.out.println("Perimetro do quadrado:" + c.getPerimetro());
        
        System.out.println("Area do retangulo:" + d.getArea());
        System.out.println("Perimetro do retangulo:" + d.getPerimetro());
        
        System.out.println("Area do triangulo:" + e.getArea());
        System.out.println("Perimetro do triangulo:" + e.getPerimetro());
    }
}
