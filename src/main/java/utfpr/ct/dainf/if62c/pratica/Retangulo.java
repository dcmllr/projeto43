/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Daniele Claudine Muller
 */
public class Retangulo implements FiguraComLados {
    double base, altura;
    
    public Retangulo() {
    
    }
    
    public Retangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    public double getArea() {
        return getLadoMaior() * getLadoMenor();
    }

    public double getPerimetro() {
        return (getLadoMaior() + getLadoMenor()) * 2;
    }
    
    @Override
    public double getLadoMenor() {
        return altura;
    }

    @Override
    public double getLadoMaior() {
        return base;
    }
}
