/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Daniele Claudine Muller
 */
public class Elipse implements FiguraComEixos {
    public double seMaior, seMenor;
    
    public Elipse(){
        
    }
    
    public Elipse(double seMaior, double seMenor){
        this.seMaior = seMaior;
        this.seMenor = seMenor;
    }
    

    public double getArea(){
        double a = Math.PI*seMaior*seMenor;
        return a;
    }
    

    public double getPerimetro(){
        double p = Math.PI * (3*(seMaior + seMenor) - Math.sqrt((3*seMaior + seMenor)*(seMaior + 3*seMenor)));
        return p;
    }
    
    @Override
    public double getEixoMenor(){
        double eMenor = 2*seMenor;
        return eMenor;
    }
    
    @Override
    public double getEixoMaior(){
        double eMaior = 2*seMaior;
        return eMaior;
    }


    public String getNome() {
        return "Elipse";
    }
}
