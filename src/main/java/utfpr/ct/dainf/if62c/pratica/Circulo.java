/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Daniele Claudine Muller
 */
public class Circulo extends Elipse {
    double  r;

    public Circulo(double r){
        super(r, r);
        this.r = r;
    }
    
    
    @Override
    public double getPerimetro(){
        double p = 2*Math.PI*r;
        return p;
    }
    
    @Override
    public String getNome() {
        return "Circulo";
    }
}
